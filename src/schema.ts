import {buildSchema} from "type-graphql";
import { Container } from "typedi";
import UserResolver from './resolvers/User'

Container.set("User", []);

 const schema = buildSchema({
    resolvers: [UserResolver],
    // here we register the auth checking function
    // or defining it inline
    // authChecker: customAuthChecker,
     container: Container,
});


export default schema;
