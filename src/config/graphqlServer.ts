import "reflect-metadata";
import {ApolloServer} from "apollo-server-express";
import schema from "../schema";

const server = new ApolloServer({
    // @ts-ignore
    schema,
    context: ({ req }) => {
        const context = {
            req,
            // @ts-ignore
            user: req.user, // `req.user` comes from `express-jwt`
        };
        return context;
    },
    playground: {
        settings: {
            'editor.theme': 'dark',
        },
    },
});

export default server;