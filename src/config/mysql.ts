import "reflect-metadata";
import {getConnectionManager} from "typeorm";
import User from "../Models/User";

export const connectToDB = async () => {
    const connectionManager = getConnectionManager();
    const dbConnection = await connectionManager.create({
        type: "mysql",
        host     : "localhost",
        username     : "root",
        database : "dvizh",
        entities: [User],
        logging: true,
        migrations: ["/migrations/*.ts"],
        cli: {
            migrationsDir: "migrations"
        }
    });
    return dbConnection.connect();
};