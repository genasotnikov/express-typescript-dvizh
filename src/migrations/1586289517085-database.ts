import {MigrationInterface, QueryRunner} from "typeorm";

export class database1586289517085 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const dbs = await queryRunner.getDatabases();
        if (!dbs.includes("dvizh")) await queryRunner.createDatabase("dvizh", true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropDatabase("dvizh", true);
    }

}
