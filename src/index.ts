import express from 'express';
import createError from'http-errors';
import jwt from "express-jwt";
import serverGQL from "./config/graphqlServer";


const app = express();

// view engine setup

app.use(express.json());
const path = "/graphql";
const GQL_PORT = 4000;

serverGQL.applyMiddleware({ app, path });

app.listen({ port: GQL_PORT }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${serverGQL.graphqlPath}`)
);

app.use(
    path,
    jwt({
      secret: "TypeGraphQL",
      credentialsRequired: false,
    }),
);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});