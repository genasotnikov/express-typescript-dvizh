import {Field, InputType} from "type-graphql";

@InputType()
class LoginArgs {
    @Field()
    email: string;

    @Field()
    password: string;
}

export default LoginArgs;