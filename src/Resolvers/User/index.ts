import {Arg, Mutation, Query, Resolver} from "type-graphql";
import LoginArgs from "./loginArgs";
import ReginsterEmailArgs from "./RegisterEmailArgs";
import User from "../../Models/User";
import UserService from "../../services/User";

@Resolver(User)
class UserResolver {
    userController = new UserService();

    @Query(returns => String)
    async login(@Arg("LoginArgs") { email, password } : LoginArgs ) {
        return await this.userController.login(email, password);
    }

    @Mutation(type => String)
    async registerThroughEmail(@Arg("ReginsterEmailArgs") {
        email, password } : ReginsterEmailArgs) {
        const newUser = new User();
        newUser.setNewUserData(email, password);
        return this.userController.addUser(newUser);
    }
}

export default UserResolver;