import jwt from "jsonwebtoken";
import User from "../../Models/User";
import { connectToDB } from '../../config/mysql';


class UserService {
    public async login(email:string, password:string): Promise<string|null> {
        const connection = await connectToDB();
        const user = await connection.getRepository(User).find({ where: { email, password } });
        await connection.close();
        console.log('user', user)
        return user.length > 0 ? jwt.sign("TypeGraphQL", email ) : "";
    }

    public async addUser(user:User) {
        const connection = await connectToDB();
        await connection.getRepository(User).save(user);
        await connection.close();
        return this.login(user.email, user.password);
    }
}

export default UserService;