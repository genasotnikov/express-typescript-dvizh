import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";
import {Field, ID, ObjectType} from "type-graphql";

@ObjectType()
@Entity()
class User {
    @PrimaryGeneratedColumn("increment")
    @Field(type => ID)
    id: number;

    @Column("varchar",{unique: true})
    @Field(type => String)
    email:string;

    @Column({ length: 40 })
    @Field(type => String)
    password:string;

    setNewUserData(email: string, password:string) {
        this.password = password;
        this.email = email;
    }
}

export default User;